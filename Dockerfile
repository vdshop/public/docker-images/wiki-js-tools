FROM php:8.1-cli

WORKDIR /usr/src/wiki-js-tools

RUN apt-get update -q \
    && apt-get install -qy git unzip \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY --from=composer:2 /usr/bin/composer /usr/bin/composer
COPY . /usr/src/wiki-js-tools

RUN ["composer", "update"]
RUN ["chmod", "+x", "bin/process-content"]

CMD ["/usr/local/bin/php", "bin/process-content"]
