<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Model;

use SplFileInfo;
use SplFileObject;
use Stringable;
use Vdshop\WikiJsTools\Model\WikiPage\Content;
use Vdshop\WikiJsTools\Model\WikiPage\Data as WikiPageData;
use Vdshop\WikiJsTools\Model\WikiPage\Metadata;
use Vdshop\WikiJsTools\Service\WikiPage\DataFactory as WikiPageDataFactory;

/**
 * Class WikiPage.
 *
 * WikiPage model.
 */
class WikiPage implements Stringable
{
    public const FILE_EXTENSION = 'md';

    /** Related objects. */
    private readonly WikiPageData $pageData;

    /**
     * WikiPage constructor.
     *
     * @param WikiPageDataFactory $wikiPageDataFactory
     * @param SplFileInfo         $fileInfo
     */
    public function __construct(
        private readonly WikiPageDataFactory $wikiPageDataFactory,
        private readonly SplFileInfo $fileInfo,
    ) {
        $this->pageData = $this->wikiPageDataFactory->create(fileContent: $this->retrieveFileContent());
    }

    /**
     * Get file info.
     *
     * @return SplFileInfo
     */
    public function getFileInfo(): SplFileInfo
    {
        return $this->fileInfo;
    }

    /**
     * @return WikiPageData
     */
    public function getPageData(): WikiPageData
    {
        return $this->pageData;
    }

    /**
     * Shortcut for getting metadata object.
     *
     * @return Metadata
     */
    public function getMetadata(): Metadata
    {
        return $this->getPageData()->getMetadata();
    }

    /**
     * Shortcut for getting content object.
     *
     * @return Content
     */
    public function getContent(): Content
    {
        return $this->getPageData()->getContent();
    }

    /**
     * String representation of current page.
     *
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->pageData;
    }

    /**
     * Retrieve file content.
     *
     * @return string
     */
    private function retrieveFileContent(): string
    {
        $fileObject = new SplFileObject(filename: $this->fileInfo->getRealPath(), mode: 'r');

        return $fileObject->getSize()
            ? $fileObject->fread(length: $fileObject->getSize())
            : '';
    }
}
