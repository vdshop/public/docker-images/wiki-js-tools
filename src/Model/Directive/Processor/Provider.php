<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Model\Directive\Processor;

use Vdshop\WikiJsTools\Contract\Directive\Processor as DirectiveProcessor;
use Vdshop\WikiJsTools\Contract\Directive\Processor\Provider as DirectiveProcessorProvider;

/**
 * Class Provider.
 *
 * Directive processor provider.
 */
class Provider implements DirectiveProcessorProvider
{
    /**
     * Provider constructor.
     *
     * @param DirectiveProcessor[] $directiveProcessors
     */
    public function __construct(
        private array $directiveProcessors = []
    ) {
        foreach ($directiveProcessors as $directiveName => $directiveProcessor) {
            // Enforce strict types checking
            $this->add(directiveName: $directiveName, directiveProcessor: $directiveProcessor);
        }
    }

    /**
     * @inheritdoc
     */
    public function get(string $directiveName): ?DirectiveProcessor
    {
        return $this->directiveProcessors[$directiveName] ?? null;
    }

    /**
     * @inheritdoc
     */
    public function getAll(): array
    {
        return $this->directiveProcessors;
    }

    /**
     * @inheritdoc
     */
    public function getDirectiveNames(): array
    {
        return \array_keys(array: $this->directiveProcessors);
    }

    /**
     * Add directive processor for directive name.
     * Used for enforcing strict types checking.
     *
     * @param string             $directiveName
     * @param DirectiveProcessor $directiveProcessor
     *
     * @return void
     */
    private function add(string $directiveName, DirectiveProcessor $directiveProcessor): void
    {
        $this->directiveProcessors[$directiveName] = $directiveProcessor;
    }
}
