<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Model\WikiPage;

use Stringable;
use Vdshop\WikiJsTools\Enum\WikiPage\FileParts as FilePartsEnum;
use Vdshop\WikiJsTools\Enum\WikiPage\MetadataKeys as MetadataKeysEnum;
use Vdshop\WikiJsTools\Service\WikiPage\ContentFactory;
use Vdshop\WikiJsTools\Service\WikiPage\MetadataFactory;

/**
 * Class Data.
 *
 * WikiPage model DTO.
 */
class Data implements Stringable
{
    /** @var Stringable[] */
    private readonly array $fileParts;

    /**
     * Data constructor.
     *
     * @param MetadataFactory   $metadataFactory
     * @param ContentFactory    $contentFactory
     * @param string|Stringable $fileContent
     */
    public function __construct(
        private readonly MetadataFactory $metadataFactory,
        private readonly ContentFactory $contentFactory,
        string|Stringable $fileContent,
    ) {
        $filePartsContent = $this->parseFileContent($fileContent);

        $metadata = $this->metadataFactory->create(metadata: $filePartsContent[FilePartsEnum::METADATA->value]);
        $content = $this->contentFactory->create(content: $filePartsContent[FilePartsEnum::PAGE_CONTENT->value]);
        
        $this->fileParts = [
            FilePartsEnum::METADATA->value     => $metadata,
            FilePartsEnum::PAGE_CONTENT->value => $content,
        ];
    }

    /**
     * Get page metadata object.
     *
     * @return Metadata
     */
    public function getMetadata(): Metadata
    {
        return $this->fileParts[FilePartsEnum::METADATA->value];
    }

    /**
     * Get page content object.
     *
     * @return Content
     */
    public function getContent(): Content
    {
        return $this->fileParts[FilePartsEnum::PAGE_CONTENT->value];
    }

    /**
     * Parse file content into parts.
     *
     * @param string|Stringable $fileContent
     *
     * @return string[]
     */
    private function parseFileContent(string|Stringable $fileContent): array
    {
        \preg_match(
            pattern: $this->getFilePartsRegex(),
            subject: $fileContent,
            matches: $fileContentMatches
        );

        return [
            FilePartsEnum::METADATA->value     => $fileContentMatches[1] ?? '',
            FilePartsEnum::PAGE_CONTENT->value => $fileContentMatches[2] ?? '',
        ];
    }

    /**
     * Get file parts regex.
     * Extracted for legibility and clarity.
     *
     * @return string
     */
    private function getFilePartsRegex(): string
    {
        $metadataKeys = MetadataKeysEnum::cases();
        $initKey = \reset(array: $metadataKeys)->value;
        $endKey = \end(array: $metadataKeys)->value;

        return '#^' .
            Metadata::METADATA_DELIMITER .
            '$\n(' .
            $initKey .
            Metadata::METADATA_LINE_EXPLODER .
            '.*' .
            $endKey .
            Metadata::METADATA_LINE_EXPLODER .
            '(?:[^\n]*))\n^' .
            Metadata::METADATA_DELIMITER .
            '$\n\n(.*?)$#msU';
    }

    /**
     * String representation of current class.
     *
     * @return string
     */
    public function __toString(): string
    {
        return \implode(
            separator: \str_repeat(string: PHP_EOL, times: 2),
            array: $this->fileParts
        );
    }
}
