<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Model\WikiPage;

use Stringable;
use Vdshop\WikiJsTools\Enum\WikiPage\Editor as EditorEnum;
use Vdshop\WikiJsTools\Enum\WikiPage\MetadataKeys;
use Vdshop\WikiJsTools\Enum\WikiPage\MetadataKeys as MetadataKeysEnum;
use Vdshop\WikiJsTools\Model\WikiPage\Metadata\Date;
use Vdshop\WikiJsTools\Model\WikiPage\Metadata\DateCreated;
use Vdshop\WikiJsTools\Model\WikiPage\Metadata\Description;
use Vdshop\WikiJsTools\Model\WikiPage\Metadata\Editor;
use Vdshop\WikiJsTools\Model\WikiPage\Metadata\Published;
use Vdshop\WikiJsTools\Model\WikiPage\Metadata\Tags;
use Vdshop\WikiJsTools\Model\WikiPage\Metadata\Title;
use Vdshop\WikiJsTools\Service\WikiPage\Metadata\DateCreatedFactory;
use Vdshop\WikiJsTools\Service\WikiPage\Metadata\DateFactory;
use Vdshop\WikiJsTools\Service\WikiPage\Metadata\DescriptionFactory;
use Vdshop\WikiJsTools\Service\WikiPage\Metadata\EditorFactory;
use Vdshop\WikiJsTools\Service\WikiPage\Metadata\PublishedFactory;
use Vdshop\WikiJsTools\Service\WikiPage\Metadata\TagsFactory;
use Vdshop\WikiJsTools\Service\WikiPage\Metadata\TitleFactory;

/**
 * Class Metadata.
 *
 * WikiPage page metadata.
 */
class Metadata implements Stringable
{
    public const METADATA_DELIMITER = '---';
    public const METADATA_LINE_EXPLODER = ': ';

    /**
     * Metadata values.
     *
     * @var Title[]|Description[]|Published[]|Date[]|Tags[]|Editor[]|DateCreated[]
     */
    private array $metadata;

    /**
     * Metadata constructor.
     *
     * @param TitleFactory       $titleFactory
     * @param DescriptionFactory $descriptionFactory
     * @param PublishedFactory   $publishedFactory
     * @param DateFactory        $dateFactory
     * @param TagsFactory        $tagsFactory
     * @param EditorFactory      $editorFactory
     * @param DateCreatedFactory $dateCreatedFactory
     * @param string|Stringable  $metadata
     */
    public function __construct(
        private readonly TitleFactory $titleFactory,
        private readonly DescriptionFactory $descriptionFactory,
        private readonly PublishedFactory $publishedFactory,
        private readonly DateFactory $dateFactory,
        private readonly TagsFactory $tagsFactory,
        private readonly EditorFactory $editorFactory,
        private readonly DateCreatedFactory $dateCreatedFactory,
        string|Stringable $metadata
    ) {
        $this->initMetadata($metadata);
    }

    /**
     * Get metadata title object.
     *
     * @return Title
     */
    public function getTitle(): Title
    {
        return $this->getByKey(MetadataKeys::TITLE);
    }

    /**
     * Get metadata title object.
     *
     * @return Description
     */
    public function getDescription(): Description
    {
        return $this->getByKey(MetadataKeys::DESCRIPTION);
    }

    /**
     * Get metadata title object.
     *
     * @return Published
     */
    public function getPublished(): Published
    {
        return $this->getByKey(MetadataKeys::PUBLISHED);
    }

    /**
     * Get metadata title object.
     *
     * @return Date
     */
    public function getDate(): Date
    {
        return $this->getByKey(MetadataKeys::DATE);
    }

    /**
     * Get metadata title object.
     *
     * @return Tags
     */
    public function getTags(): Tags
    {
        return $this->getByKey(MetadataKeys::TAGS);
    }

    /**
     * Get metadata title object.
     *
     * @return Editor
     */
    public function getEditor(): Editor
    {
        return $this->getByKey(MetadataKeys::EDITOR);
    }

    /**
     * Get metadata title object.
     *
     * @return DateCreated
     */
    public function getDateCreated(): DateCreated
    {
        return $this->getByKey(MetadataKeys::DATE_CREATED);
    }

    /**
     * Get metadata by key.
     *
     * @param MetadataKeysEnum $metadataKey
     *
     * @return Title|Description|Published|Date|Tags|Editor|DateCreated
     */
    public function getByKey(MetadataKeysEnum $metadataKey): Title|Description|Published|Date|Tags|Editor|DateCreated
    {
        return $this->metadata[$metadataKey->value];
    }

    /**
     * Init metadata array from string.
     *
     * @param string|Stringable $metadata
     *
     * @return void
     */
    private function initMetadata(string|Stringable $metadata): void
    {
        $this->metadata = $this->filterMetadata(
            metadata: $this->parseMetadata(metadata: $metadata)
        );

        $this->metadata[MetadataKeysEnum::TITLE->value] = $this->titleFactory->create(
            title: $this->metadata[MetadataKeysEnum::TITLE->value]
        );

        $this->metadata[MetadataKeysEnum::DESCRIPTION->value] = $this->descriptionFactory->create(
            description: $this->metadata[MetadataKeysEnum::DESCRIPTION->value]
        );

        $this->metadata[MetadataKeysEnum::PUBLISHED->value] = $this->publishedFactory->create(
            published: $this->metadata[MetadataKeysEnum::PUBLISHED->value]
        );

        $this->metadata[MetadataKeysEnum::DATE->value] = $this->dateFactory->create(
            date: $this->metadata[MetadataKeysEnum::DATE->value]
        );

        $this->metadata[MetadataKeysEnum::TAGS->value] = $this->tagsFactory->create(
            tags: $this->metadata[MetadataKeysEnum::TAGS->value]
        );

        $this->metadata[MetadataKeysEnum::EDITOR->value] = $this->editorFactory->create(
            editor: EditorEnum::tryFrom($this->metadata[MetadataKeysEnum::EDITOR->value])
        );

        $this->metadata[MetadataKeysEnum::DATE_CREATED->value] = $this->dateCreatedFactory->create(
            dateCreated: $this->metadata[MetadataKeysEnum::DATE_CREATED->value]
        );
    }

    /**
     * Parse metadata string to metadata array.
     *
     * @param string|Stringable $metadata
     *
     * @return string[]
     */
    private function parseMetadata(string|Stringable $metadata): array
    {
        $metadata = \array_reduce(
            array: $this->getMetadataLines(metadata: $metadata),
            callback: $this->addLineToMetadata(...),
            initial: []
        );

        foreach (MetadataKeysEnum::cases() as $metadataKey) {
            $metadata[$metadataKey->value] ??= '';
        }

        return $metadata;
    }

    /**
     * Get metadata lines.
     *
     * @return string[]
     */
    private function getMetadataLines(string|Stringable $metadata): array
    {
        return \explode(separator: PHP_EOL, string: $metadata);
    }

    /**
     * Parse metadata line.
     * Returns metadata line array when line can be processed in format: [metadataKey => metadataValue].
     * Returns null otherwise.
     *
     * @param string[] $metadata
     * @param string   $metadataLine
     *
     * @return string[]
     */
    private function addLineToMetadata(array $metadata, string $metadataLine): array
    {
        if (\preg_match(
            pattern: '#^(\w+)' . self::METADATA_LINE_EXPLODER . '(.*)$#',
            subject: $metadataLine,
            matches: $lineMatches
        )) {
            $metadata[\trim(string: $lineMatches[1])] = \trim(string: $lineMatches[2]);
        }

        return $metadata;
    }

    /**
     * Filter metadata.
     *
     * @param string[] $metadata
     *
     * @return string[]
     */
    private function filterMetadata(array $metadata): array
    {
        return \array_filter(
            array: $metadata,
            callback: static fn(string $value, string $key): bool => null !== MetadataKeysEnum::tryFrom(value: $key),
            mode: ARRAY_FILTER_USE_BOTH
        );
    }

    /**
     * String representation of current class.
     *
     * @return string
     */
    public function __toString(): string
    {
        $metadataLines = \array_reduce(
            array:    MetadataKeysEnum::cases(),
            callback: function (array $metadataLines, MetadataKeysEnum $metadataKey): array {
                $metadataLines[] = $metadataKey->value . self::METADATA_LINE_EXPLODER . $this->getByKey($metadataKey);
                return $metadataLines;
            },
            initial: []
        );

        return \implode(
            separator: PHP_EOL,
            array:     \array_merge([self::METADATA_DELIMITER], $metadataLines, [self::METADATA_DELIMITER])
        );
    }
}
