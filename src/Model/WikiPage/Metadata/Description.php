<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Model\WikiPage\Metadata;

use Stringable;

/**
 * Class Description.
 *
 * WikiPage description.
 */
class Description implements Stringable
{
    /**
     * Description constructor.
     *
     * @param string|Stringable $description
     */
    public function __construct(
        private string|Stringable $description,
    ) {
    }

    /**
     * Get description.
     *
     * @return string|Stringable
     */
    public function get(): string|Stringable
    {
        return $this->description;
    }

    /**
     * @param string|Stringable $description
     */
    public function set(Stringable|string $description): void
    {
        $this->description = $description;
    }

    /**
     * String representation of current class.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->get();
    }
}
