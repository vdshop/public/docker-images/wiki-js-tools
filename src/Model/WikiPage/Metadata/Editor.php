<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Model\WikiPage\Metadata;

use Stringable;
use Vdshop\WikiJsTools\Enum\WikiPage\Editor as EditorEnum;

/**
 * Class Editor.
 *
 * WikiPage editor.
 */
class Editor implements Stringable
{
    /**
     * Editor constructor.
     *
     * @param EditorEnum|null $editor
     */
    public function __construct(
        private ?EditorEnum $editor,
    ) {
    }

    /**
     * Get editor.
     *
     * @return EditorEnum
     */
    public function get(): EditorEnum
    {
        return $this->editor ??= EditorEnum::MARKDOWN;
    }

    /**
     * Set editor.
     *
     * @param EditorEnum $editor
     */
    public function set(EditorEnum $editor): void
    {
        $this->editor = $editor;
    }

    /**
     * String representation of current class.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->get()->value;
    }
}
