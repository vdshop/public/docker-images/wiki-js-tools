<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Model\WikiPage\Metadata;

use Stringable;
use Vdshop\WikiJsTools\Enum\WikiPage\TagKeys;
use Vdshop\WikiJsTools\Enum\WikiPage\TagKeys as TagKeysEnum;

/**
 * Class Tags.
 *
 * WikiPage tags.
 */
class Tags implements Stringable
{
    public const TAG_SEPARATOR = ', ';

    /**
     * Tag values.
     *
     * @var array[]
     */
    private array $tags;

    /**
     * Tags constructor.
     *
     * @param string|Stringable $tags
     * @param string|Stringable $tagExploder
     */
    public function __construct(
        string|Stringable $tags = '',
        private readonly string|Stringable $tagExploder = ' - ',
    ) {
        $this->setFromString(tags: $tags);
    }

    /**
     * Set tag(s) from string.
     *
     * @param string|Stringable $tags
     *
     * @return void
     */
    public function setFromString(string|Stringable $tags): void
    {
        $this->tags = [];

        $this->addFromString(tags: $tags);
    }


    /**
     * Add tag(s) from string.
     *
     * @param string|Stringable $tags
     *
     * @return void
     */
    public function addFromString(string|Stringable $tags): void
    {
        \array_map(
            array: \array_map(
                       array: \explode(separator: self::TAG_SEPARATOR, string: $tags),
                       callback: static fn(string $tag) => \trim($tag)
                   ),
            callback: function (string $tagString) {
                if (\str_contains(haystack: $tagString, needle: $this->tagExploder)) {
                    [$tagKey, $tagValue] = \explode(separator: $this->tagExploder, string: $tagString, limit: 2);
                    if (null !== TagKeysEnum::tryFrom($tagKey)) {
                        $this->addByKey(tagKey: TagKeysEnum::from($tagKey), tagValues: $tagValue);
                    }
                }
            },
        );

        $this->filterTags();
    }

    /**
     * Get tags by key.
     *
     * @param TagKeysEnum $tagKey
     *
     * @return string[]
     */
    public function getByKey(TagKeysEnum $tagKey): array
    {
        return $this->tags[$tagKey->value] ??= [];
    }

    /**
     * Add tag(s) by key.
     *
     * @param TagKeysEnum     $tagKey
     * @param string|string[] $tagValues
     *
     * @return void
     */
    public function addByKey(TagKeysEnum $tagKey, string|array $tagValues): void
    {
        $tagValues = \array_filter(
            array: (array)$tagValues,
            callback: static fn (?string $tagValue) => '' !== (string)$tagValue,
        );

        if ([] === $tagValues) {
            return;
        }

        $this->tags[$tagKey->value] = \array_unique(
            array: \array_merge($this->getByKey($tagKey), $tagValues)
        );

        \ksort(array: $this->tags[$tagKey->value]);
        \ksort(array: $this->tags);

        $this->filterTags();
    }

    /**
     * Set tag(s) by key.
     *
     * @param TagKeysEnum     $tagKey
     * @param string|string[] $tagValues
     *
     * @return void
     */
    public function setByKey(TagKeysEnum $tagKey, string|array $tagValues): void
    {
        $this->tags[$tagKey->value] = [];
        $this->addByKey($tagKey, $tagValues);
    }

    /**
     * Unset tag(s) by key.
     *
     * @param TagKeysEnum     $tagKey
     *
     * @return void
     */
    public function unsetByKey(TagKeysEnum $tagKey): void
    {
        if (\array_key_exists($tagKey->value, $this->tags)) {
            unset($this->tags[$tagKey->value]);
        }
    }

    /**
     * Return flattened tags.
     *
     * @return string[]
     */
    public function get(): array
    {
        $tagStrings = [];

        foreach ($this->tags as $tagKey => $tagValues) {
            foreach ($tagValues as $tagValue) {
                $tagStrings[] = $tagKey . $this->tagExploder . $tagValue;
            }
        }

        return $tagStrings;
    }

    /**
     * Get tags link.
     *
     * @return string
     */
    public function getLink(): string
    {
        $tagsLink = \array_reduce(
            array:   $this->get(),
            callback: static fn(string $link, string $tag) => $link . \rawurlencode(string: $tag) . '/',
            initial: '/t/'
        );

        return $tagsLink . '?sort=title';
    }

    /**
     * @return string|Stringable
     */
    public function getTagExploder(): Stringable|string
    {
        return $this->tagExploder;
    }

    /**
     * Filter tags.
     *
     * @return void
     */
    private function filterTags(): void
    {
        $this->tags = \array_filter(
            array: $this->tags,
            callback: $this->filterTag(...),
            mode: ARRAY_FILTER_USE_BOTH
        );
    }

    /**
     * Filter allowed tags and values.
     *
     * @param string[]|null $value
     * @param string        $key
     *
     * @return bool
     */
    private function filterTag(?array $value, string $key): bool
    {
        return [] !== $value && null !== TagKeysEnum::tryFrom(value: $key);
    }

    /**
     * String representation of current class.
     *
     * @return string
     */
    public function __toString(): string
    {
        return \implode(
            separator: self::TAG_SEPARATOR,
            array: $this->get()
        );
    }
}
