<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Model\WikiPage\Metadata;

use Stringable;

/**
 * Class Title.
 *
 * WikiPage title.
 */
class Title implements Stringable
{
    /**
     * Title constructor.
     *
     * @param string|Stringable $title
     */
    public function __construct(
        private string|Stringable $title,
    ) {
    }

    /**
     * Get title.
     *
     * @return string|Stringable
     */
    public function get(): string|Stringable
    {
        return $this->title;
    }

    /**
     * Set title.
     *
     * @param string|Stringable $title
     */
    public function set(Stringable|string $title): void
    {
        $this->title = $title;
    }

    /**
     * String representation of current class.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->get();
    }
}
