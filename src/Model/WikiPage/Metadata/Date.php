<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Model\WikiPage\Metadata;

use DateTime;
use DateTimeInterface;
use Stringable;

/**
 * Class Date.
 *
 * WikiPage date.
 */
class Date implements Stringable
{
    public const DATE_FORMAT = 'Y-m-d\TH:i:s.v\Z';

    /**
     * @var DateTime
     */
    private DateTime $date;

    /**
     * Date constructor.
     *
     * @param string|Stringable|DateTimeInterface|null $date
     */
    public function __construct(
        string|Stringable|DateTimeInterface|null $date = null,
    ) {
        $this->set(date: $date);
    }

    /**
     * Get date as string.
     *
     * @return string|Stringable
     */
    public function get(): string|Stringable
    {
        return $this->getDateTime()->format(format: self::DATE_FORMAT);
    }

    /**
     * Set date.
     *
     * @param string|Stringable|DateTimeInterface|null $date
     *
     * @return void
     */
    public function set(string|Stringable|DateTimeInterface|null $date = null): void
    {
        if (\is_null(value: $date) || \is_string(value: $date) || $date instanceof Stringable) {
            $date = DateTime::createFromFormat(
                datetime: (string)$date,
                format: self::DATE_FORMAT,
            ) ?: new DateTime;
        }

        $this->date = DateTime::createFromInterface(object: $date);
    }

    /**
     * Get date as DateTime.
     *
     * @return DateTime
     */
    public function getDateTime(): DateTime
    {
        return $this->date;
    }

    /**
     * String representation of current class.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->get();
    }
}
