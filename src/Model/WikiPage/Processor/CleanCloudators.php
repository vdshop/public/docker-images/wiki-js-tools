<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Model\WikiPage\Processor;

use Vdshop\WikiJsTools\Contract\Logger;
use Vdshop\WikiJsTools\Contract\WikiPage\Processor as WikiPageProcessor;
use Vdshop\WikiJsTools\Model\WikiPage;
use Vdshop\WikiJsTools\Service\FeatureFlag;
use Vdshop\WikiJsTools\Traits\Path\IsFeatureFlagEnabled;

/**
 * Class CleanCloudators.
 *
 * Clean unwanted cloudators.
 */
class CleanCloudators implements WikiPageProcessor
{
    use IsFeatureFlagEnabled;

    public const  DEFAULT_CLOUDATOR_OPEN  = '{{';
    public const  DEFAULT_CLOUDATOR_CLOSE = '}}';
    private const FF_NAME                 = 'CLEAN_CLOUDATORS';

    /**
     * CleanCloudators constructor.
     *
     * @param FeatureFlag $featureFlag
     * @param Logger      $logger
     * @param string      $cloudatorOpen
     * @param string      $cloudatorClose
     */
    public function __construct(
        private readonly FeatureFlag $featureFlag,
        private readonly Logger $logger,
        private readonly string $cloudatorOpen = self::DEFAULT_CLOUDATOR_OPEN,
        private readonly string $cloudatorClose = self::DEFAULT_CLOUDATOR_CLOSE,
    ) {
    }

    /**
     * @inheritdoc
     */
    public function execute(WikiPage $wikiPage): void
    {
        if (!$this->supports(wikiPage: $wikiPage)) {
            $this->logger->debug(
                message: \get_class($this) .
                         ' does not support ' .
                         $wikiPage->getFileInfo()->getType() .
                         ' ' .
                         $wikiPage->getFileInfo()->getRealPath()
            );

            return;
        }

        if (!$this->isFeatureFlagEnabled()) {
            $this->logger->debug(
                message: \get_class($this) .
                         ' skipped, feature flag ' .
                         FeatureFlag::DEFAULT_PREFIX .
                         self::FF_NAME .
                         ' not enabled.'
            );

            return;
        }

        $origContent = $wikiPage->getContent()->get();

        $processedContent = \preg_replace(
            pattern:     '#' . $this->cloudatorOpen . '.*' . $this->cloudatorClose . '#U',
            replacement: '',
            subject:     $origContent,
        );

        if ($origContent !== $processedContent) {
            $wikiPage->getContent()->set($processedContent);
            $this->logger->notice(message: '[!] ' . self::FF_NAME . ' updated content');
            $this->logger->increaseIndentation();
            $this->logger->debug(message: 'from: ' . $origContent);
            $this->logger->debug(message: 'to  : ' . $processedContent);
            $this->logger->decreaseIndentation();
        }
    }

    /**
     * Check if this processor supports given wiki page.
     *
     * @param WikiPage $wikiPage
     *
     * @return bool
     */
    private function supports(WikiPage $wikiPage): bool
    {
        return $wikiPage->getFileInfo()->isFile();
    }
}
