<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Model\WikiPage\Processor;

use Vdshop\WikiJsTools\Contract\WikiPage\Processor as WikiPageProcessor;
use Vdshop\WikiJsTools\Model\WikiPage as WikiPageModel;

/**
 * Class Composite.
 */
class Composite implements WikiPageProcessor
{
    private array $wikiPageProcessors;

    /**
     * Class Composite.
     *
     * @param WikiPageProcessor ...$wikiPageProcessors
     */
    public function __construct(
        WikiPageProcessor ...$wikiPageProcessors
    ) {
        // Variadic arguments cannot be promoted.
        $this->wikiPageProcessors = $wikiPageProcessors;
    }

    /**
     * @inheritdoc
     */
    public function execute(WikiPageModel $wikiPage): void
    {
        \array_map(
            array:    $this->wikiPageProcessors,
            callback: static fn(WikiPageProcessor $wikiPageProcessor) => $wikiPageProcessor->execute(wikiPage: $wikiPage),
        );
    }
}
