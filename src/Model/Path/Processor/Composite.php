<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Model\Path\Processor;

use SplFileInfo;
use Vdshop\WikiJsTools\Contract\Path\Processor as PathProcessor;

/**
 * Class Composite.
 *
 * Performs some path processing by delegating it to a set of given path processors.
 */
class Composite implements PathProcessor
{
    /**
     * @var PathProcessor[]
     */
    private readonly array $pathProcessors;

    /**
     * Composite constructor.
     *
     * @param PathProcessor ...$pathProcessors
     */
    public function __construct(
        PathProcessor ...$pathProcessors
    ) {
        // Variadic arguments cannot be promoted.
        $this->pathProcessors = $pathProcessors;
    }

    /**
     * @inheritdoc
     */
    public function execute(SplFileInfo $fileInfo): void
    {
        \array_map(
            array: $this->pathProcessors,
            callback: static fn(PathProcessor $pathProcessor) => $pathProcessor->execute($fileInfo),
        );
    }
}
