<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Enum\Content;

/**
 * Enum PageTypes.
 */
enum PageTypes: string
{
    case INDEX = 'index';
    case CONTENT = 'content';
}
