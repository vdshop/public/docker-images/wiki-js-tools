<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Enum\WikiPage;

/**
 * Enum Editor.
 *
 * Allowed editor values.
 */
enum Editor: string
{
    case MARKDOWN = 'markdown';
    case WYSIWYG = 'ckeditor';
    case RAW = 'code';
}
