<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Enum\WikiPage;

/**
 * Enum FileParts.
 *
 * WikiPage file parts.
 */
enum FileParts: string
{
    case METADATA = 'metadata';
    case PAGE_CONTENT = 'page_content';
}
