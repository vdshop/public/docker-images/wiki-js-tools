<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service;

/**
 * Class FeatureFlag.
 *
 * Provide feature flags status and values.
 */
class FeatureFlag
{
    public const  DEFAULT_PREFIX        = 'FF_';
    private const DEFAULT_TRUTHY_VALUES = [
        '1',
        'true',
        'yes',
        'on',
    ];

    /**
     * FeatureFlag constructor.
     *
     * @param string   $featureFlagPrefix
     * @param string[] $truthyValues
     */
    public function __construct(
        private readonly string $featureFlagPrefix = self::DEFAULT_PREFIX,
        private readonly array $truthyValues = self::DEFAULT_TRUTHY_VALUES,
    ) {
    }

    /**
     * Check feature flag is enabled.
     *
     * @param string      $featureFlagName
     * @param string|null $default
     *
     * @return bool
     */
    public function isEnabled(string $featureFlagName, string $default = null): bool
    {
        return \in_array(
            needle:   $this->getValue($featureFlagName, $default) ?? '',
            haystack: $this->truthyValues,
            strict:   true
        );
    }

    /**
     * Get feature flag config value.
     *
     * @param string      $featureFlagName
     * @param string|null $default
     *
     * @return string|null
     */
    public function getValue(string $featureFlagName, string $default = null): ?string
    {
        $value = \getenv(name: $this->featureFlagPrefix . $featureFlagName);

        return '' === $value || false === $value ? $default : (string)$value;
    }
}
