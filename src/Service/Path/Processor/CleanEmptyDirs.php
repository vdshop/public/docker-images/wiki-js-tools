<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\Path\Processor;

use SplFileInfo;
use Vdshop\WikiJsTools\Contract\Logger;
use Vdshop\WikiJsTools\Contract\Path\Processor as PathProcessor;
use Vdshop\WikiJsTools\Service\FeatureFlag;
use Vdshop\WikiJsTools\Service\Provider\Filesystem\Iterator;
use Vdshop\WikiJsTools\Service\WikiPageFactory;
use Vdshop\WikiJsTools\Traits\Path\GetRelativePath;
use Vdshop\WikiJsTools\Traits\Path\IsFeatureFlagEnabled;

/**
 * Class CleanEmptyDirs.
 *
 * Class name is self-descriptive.
 */
class CleanEmptyDirs implements PathProcessor
{
    use GetRelativePath;
    use IsFeatureFlagEnabled;

    private const FF_NAME = 'CLEAN_EMPTY_DIRS';

    /**
     * CleanEmptyDirs constructor.
     *
     * @param WikiPageFactory $wikiPageFactory
     * @param FeatureFlag     $featureFlag
     * @param Iterator        $filesystemIterator
     * @param Logger          $logger
     */
    public function __construct(
        private readonly WikiPageFactory $wikiPageFactory,
        private readonly FeatureFlag $featureFlag,
        private readonly Iterator $filesystemIterator,
        private readonly Logger $logger,
    ) {
    }

    /**
     * @inheritdoc
     */
    public function execute(SplFileInfo $fileInfo): void
    {
        if (!$this->supports(fileInfo: $fileInfo)) {
            $this->logger->debug(
                message: \get_class($this) .
                         ' does not support ' .
                         $fileInfo->getType() .
                         ' ' .
                         $fileInfo->getRealPath()
            );

            return;
        }

        if (!$this->isFeatureFlagEnabled()) {
            $this->logger->debug(
                message: \get_class($this) .
                         ' skipped, feature flag ' .
                         FeatureFlag::DEFAULT_PREFIX .
                         self::FF_NAME .
                         ' not enabled.'
            );

            return;
        }

        $filePath = $fileInfo->getRealPath();

        \rmdir(directory: $filePath);

        $this->logger->notice(
            message: '[!] Deleted empty directory: ' . $this->getRelativePath(absolutePath: $filePath)
        );
    }

    /**
     * Check if this processor supports given file info.
     *
     * @param SplFileInfo $fileInfo
     *
     * @return bool
     */
    private function supports(SplFileInfo $fileInfo): bool
    {
        if (!$fileInfo->isDir()) {
            return false;
        }

        $dirFiles = $this->filesystemIterator->getFilesArray($fileInfo->getRealPath());
        $dirDirs = $this->filesystemIterator->getDirsArray($fileInfo->getRealPath());

        return 0 === \count($dirFiles) && 0 === \count($dirDirs);
    }
}
