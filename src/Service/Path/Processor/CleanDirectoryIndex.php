<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\Path\Processor;

use SplFileInfo;
use Vdshop\WikiJsTools\Contract\Logger;
use Vdshop\WikiJsTools\Contract\Path\Processor as PathProcessor;
use Vdshop\WikiJsTools\Enum\Content\PageTypes as PageTypesEnum;
use Vdshop\WikiJsTools\Enum\WikiPage\TagKeys as TagKeysEnum;
use Vdshop\WikiJsTools\Model\WikiPage;
use Vdshop\WikiJsTools\Service\FeatureFlag;
use Vdshop\WikiJsTools\Service\Provider\Filesystem\Iterator;
use Vdshop\WikiJsTools\Service\WikiPageFactory;
use Vdshop\WikiJsTools\Traits\Path\GetRelativePath;
use Vdshop\WikiJsTools\Traits\Path\IsFeatureFlagEnabled;

/**
 * Class CleanDirectoryIndex.
 *
 * Class name is self-descriptive.
 */
class CleanDirectoryIndex implements PathProcessor
{
    use GetRelativePath;
    use IsFeatureFlagEnabled;

    private const FF_NAME = 'CLEAN_DIRECTORY_INDEX';

    /**
     * CleanDirectoryIndex constructor.
     *
     * @param WikiPageFactory $wikiPageFactory
     * @param FeatureFlag     $featureFlag
     * @param Iterator        $filesystemIterator
     * @param Logger          $logger
     */
    public function __construct(
        private readonly WikiPageFactory $wikiPageFactory,
        private readonly FeatureFlag $featureFlag,
        private readonly Iterator $filesystemIterator,
        private readonly Logger $logger,
    ) {
    }

    /**
     * @inheritdoc
     */
    public function execute(SplFileInfo $fileInfo): void
    {
        if (!$this->supports(fileInfo: $fileInfo)) {
            $this->logger->debug(
                message: \get_class($this) .
                         ' does not support ' .
                         $fileInfo->getType() .
                         ' ' .
                         $fileInfo->getRealPath()
            );

            return;
        }

        if (!$this->isFeatureFlagEnabled()) {
            $this->logger->debug(
                message: \get_class($this) .
                         ' skipped, feature flag ' .
                         FeatureFlag::DEFAULT_PREFIX .
                         self::FF_NAME .
                         ' not enabled.'
            );

            return;
        }

        $filePath = $fileInfo->getRealPath();

        \unlink(filename: $filePath);

        $this->logger->notice(
            message: '[!] Deleted directory index: ' . $this->getRelativePath(absolutePath: $filePath)
        );
    }

    /**
     * Check if this processor supports given file info.
     *
     * @param SplFileInfo $fileInfo
     *
     * @return bool
     */
    private function supports(SplFileInfo $fileInfo): bool
    {
        if (!$fileInfo->isFile() || $fileInfo->getExtension() !== WikiPage::FILE_EXTENSION) {
            return false;
        }

        $wikiPage = $this->wikiPageFactory->create(fileInfo: $fileInfo);
        $pageType = $wikiPage->getMetadata()->getTags()->getByKey(tagKey: TagKeysEnum::PAGE_TYPE);

        if (PageTypesEnum::INDEX->value !== \reset(array: $pageType)) {
            return false;
        }

        $relatedDirPath = $fileInfo->getPath() . DIRECTORY_SEPARATOR . $fileInfo->getBasename(
            suffix: '.' . WikiPage::FILE_EXTENSION
        );

        return $this->featureFlag->isEnabled(self::FF_NAME . '_FORCE')
            || !\is_dir(filename: $relatedDirPath)
            || 0 === \count(
                value: $this->filesystemIterator->getFilesArray(
                    path: $relatedDirPath,
                    callbackFilter: static fn(SplFileInfo $currentFileInfo): bool =>
                        $currentFileInfo->getExtension() === WikiPage::FILE_EXTENSION
                )
            );
    }
}
