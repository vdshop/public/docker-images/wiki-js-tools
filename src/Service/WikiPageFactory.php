<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service;

use SplFileInfo;
use Vdshop\WikiJsTools\Model\WikiPage as WikiPageModel;

/**
 * Interface WikiPageFactory.
 *
 * WikiPage model factory.
 */
interface WikiPageFactory
{
    /**
     * Create WikiPage model.
     *
     * @param SplFileInfo $fileInfo
     */
    public function create(SplFileInfo $fileInfo): WikiPageModel;
}
