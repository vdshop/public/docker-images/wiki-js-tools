<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\WikiPage;

use Stringable;
use Vdshop\WikiJsTools\Model\WikiPage\Data;

/**
 * Interface DataFactory.
 *
 * WikiPage model DTO factory.
 */
interface DataFactory
{
    /**
     * Create WikiPage data object.
     *
     * @param string|Stringable $fileContent
     */
    public function create(string|Stringable $fileContent): Data;
}
