<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\WikiPage\Metadata;

use DateTimeInterface;
use Stringable;
use Vdshop\WikiJsTools\Model\WikiPage\Metadata\Date;

/**
 * Interface DateFactory.
 *
 * WikiPage model date factory.
 */
interface DateFactory
{
    /**
     * Create WikiPage date object.
     */
    public function create(string|Stringable|DateTimeInterface|null $date = null): Date;
}
