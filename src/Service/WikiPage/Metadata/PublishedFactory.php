<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\WikiPage\Metadata;

use Stringable;
use Vdshop\WikiJsTools\Model\WikiPage\Metadata\Published;

/**
 * Interface PublishedFactory.
 *
 * WikiPage model published factory.
 */
interface PublishedFactory
{
    /**
     * Create WikiPage published object.
     */
    public function create(string|Stringable|bool $published = true): Published;
}
