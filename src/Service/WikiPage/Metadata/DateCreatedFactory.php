<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\WikiPage\Metadata;

use DateTimeInterface;
use Stringable;
use Vdshop\WikiJsTools\Model\WikiPage\Metadata\DateCreated;

/**
 * Interface DateCreatedFactory.
 *
 * WikiPage model date created factory.
 */
interface DateCreatedFactory
{
    /**
     * Create WikiPage date created object.
     */
    public function create(string|Stringable|DateTimeInterface|null $dateCreated = null): DateCreated;
}
