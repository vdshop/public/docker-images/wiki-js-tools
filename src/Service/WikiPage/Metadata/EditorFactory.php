<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\WikiPage\Metadata;

use Vdshop\WikiJsTools\Enum\WikiPage\Editor as EditorEnum;
use Vdshop\WikiJsTools\Model\WikiPage\Metadata\Editor;

/**
 * Interface EditorFactory.
 *
 * WikiPage model editor factory.
 */
interface EditorFactory
{
    /**
     * Create WikiPage editor object.
     */
    public function create(?EditorEnum $editor): Editor;
}
