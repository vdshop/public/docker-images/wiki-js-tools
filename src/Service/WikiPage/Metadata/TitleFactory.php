<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\WikiPage\Metadata;

use Stringable;
use Vdshop\WikiJsTools\Model\WikiPage\Metadata\Title;

/**
 * Interface TitleFactory.
 *
 * WikiPage model title factory.
 */
interface TitleFactory
{
    /**
     * Create WikiPage title object.
     */
    public function create(string|Stringable $title): Title;
}
