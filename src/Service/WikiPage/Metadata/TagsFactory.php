<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\WikiPage\Metadata;

use Stringable;
use Vdshop\WikiJsTools\Model\WikiPage\Metadata\Tags;

/**
 * Interface TagsFactory.
 *
 * WikiPage model tags factory.
 */
interface TagsFactory
{
    /**
     * Create WikiPage tags object.
     */
    public function create(
        string|Stringable $tags = '',
        string|Stringable $tagExploder = ' - ',
    ): Tags;
}
