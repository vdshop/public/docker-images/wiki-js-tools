<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\WikiPage;

use Stringable;
use Vdshop\WikiJsTools\Model\WikiPage\Content;

/**
 * Interface ContentFactory.
 *
 * WikiPage model content factory.
 */
interface ContentFactory
{
    /**
     * Create WikiPage content object.
     *
     * @param string|Stringable $content
     */
    public function create(string|Stringable $content): Content;
}
