<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\WikiPage\Processor;

use Vdshop\WikiJsTools\Contract\Directive\Processor\Provider as DirectiveProcessorProvider;
use Vdshop\WikiJsTools\Contract\Logger;
use Vdshop\WikiJsTools\Contract\WikiPage\Processor as WikiPageProcessor;
use Vdshop\WikiJsTools\Model\WikiPage;

/**
 * Class Directives.
 *
 * Process content directives.
 */
class Directives implements WikiPageProcessor
{
    public const DIRECTIVE_OPEN = '{.';
    public const DIRECTIVE_CLOSE = '}';
    public const DIRECTIVE_START_SUFFIX = '-start';
    public const DIRECTIVE_END_SUFFIX = '-end';
    private const RECURSION_LIMIT = 20;
    private const REGEX_DELIMITER = '#';

    private string $directive_open;
    private string $directive_close;
    private string $directive_start_suffix;
    private string $directive_end_suffix;

    /**
     * Directives constructor.
     *
     * @param DirectiveProcessorProvider $directiveProvider
     * @param Logger                     $logger
     */
    public function __construct(
        private readonly DirectiveProcessorProvider $directiveProvider,
        private readonly Logger $logger,
    ) {
        $this->directive_open = \preg_quote(str: self::DIRECTIVE_OPEN, delimiter: self::REGEX_DELIMITER);
        $this->directive_close = \preg_quote(str: self::DIRECTIVE_CLOSE, delimiter: self::REGEX_DELIMITER);
        $this->directive_start_suffix = \preg_quote(str: self::DIRECTIVE_START_SUFFIX, delimiter: self::REGEX_DELIMITER);
        $this->directive_end_suffix = \preg_quote(str: self::DIRECTIVE_END_SUFFIX, delimiter: self::REGEX_DELIMITER);
    }

    /**
     * @inheritdoc
     */
    public function execute(WikiPage $wikiPage): void
    {
        if (!$this->supports(wikiPage: $wikiPage)) {
            return;
        }

        $content = $wikiPage->getContent()->get();
        $originalContent = $content;

        $iterationCount = 0;
        do {
            $iterationInitialContent = $content;
            $iterationCount++;
            $content = $this->refreshProcessedDirectives(content: $content, wikiPage: $wikiPage);
            $content = $this->processUnprocessedDirectives($content, $wikiPage);
            $isContentChangedDuringIteration = $iterationInitialContent !== $content;
        } while($isContentChangedDuringIteration && $iterationCount < self::RECURSION_LIMIT);

        if ($originalContent !== $content) {
            $wikiPage->getContent()->set(content: $content);
            $this->logger->notice(message: '[!] Updated content from directives');
        }
    }

    /**
     * Check if this processor supports given wiki page.
     *
     * @param WikiPage $wikiPage
     *
     * @return bool
     */
    private function supports(WikiPage $wikiPage): bool
    {
        $fileInfo = $wikiPage->getFileInfo();

        return $fileInfo->isFile() && $fileInfo->getExtension() === WikiPage::FILE_EXTENSION;
    }

    /**
     * Refresh already processed directives that have start and end tags and content inside.
     *
     * @param string   $content
     * @param WikiPage $wikiPage
     *
     * @return string
     */
    private function refreshProcessedDirectives(string $content, WikiPage $wikiPage): string
    {
        if (\preg_match_all(pattern: $this->getDirectiveStartEndRegex(), subject: $content, matches: $matches)) {
            foreach ($matches[1] as $index => $directiveName) {
                $rawDirectiveArgs = $matches[3][$index];
                $content = \preg_replace(
                    pattern:     $this->getDirectiveStartEndRegex(
                               directiveName:    $directiveName,
                               rawDirectiveArgs: $rawDirectiveArgs,
                           ),
                    replacement: $this->getDirectiveContent(
                                     directiveName:    $directiveName,
                                     rawDirectiveArgs: $rawDirectiveArgs,
                                     directiveArgs:    $this->parseDirectiveArgs(rawDirectiveArgs: $rawDirectiveArgs),
                                     wikiPage:         $wikiPage
                                 ),
                    subject:     $content,
                );
            }
        }

        return $content;
    }

    /**
     * Process directives that have not been processed yet and have a single declaration tag.
     *
     * @param string   $content
     * @param WikiPage $wikiPage
     *
     * @return string
     */
    private function processUnprocessedDirectives(string $content, WikiPage $wikiPage): string
    {
        if (\preg_match_all(pattern: $this->getDirectiveRegex(), subject: $content, matches: $matches)) {
            foreach ($matches[1] as $index => $directiveName) {
                $rawDirectiveArgs = $matches[3][$index];
                $content = \preg_replace(
                    pattern:     $this->getDirectiveRegex(
                               directiveName:    $directiveName,
                               rawDirectiveArgs: $rawDirectiveArgs,
                           ),
                    replacement: $this->getDirectiveContent(
                                     directiveName:    $directiveName,
                                     rawDirectiveArgs: $rawDirectiveArgs,
                                     directiveArgs:    $this->parseDirectiveArgs(rawDirectiveArgs: $rawDirectiveArgs),
                                     wikiPage:         $wikiPage
                                 ),
                    subject:     $content,
                );
            }
        }

        return $content;
    }

    /**
     * Regex for matching and capturing directive name and args when directive has not been processed yet.
     *
     * @param string|null $directiveName
     * @param string|null $rawDirectiveArgs
     *
     * @return string
     */
    private function getDirectiveRegex(string $directiveName = null, string $rawDirectiveArgs = null): string
    {
        $directiveName = \is_string($directiveName)
            ? \preg_quote(str: $directiveName, delimiter: self::REGEX_DELIMITER)
            : $this->getDirectiveNamesRegex();
        $rawDirectiveArgs ??= '.*';

        return self::REGEX_DELIMITER .
            '^' . $this->directive_open .
            '(' . $directiveName . ')' .
            '(?:()' . $this->directive_close . '|\s+?(' . $rawDirectiveArgs . ')' . $this->directive_close . ')\s*$' .
            self::REGEX_DELIMITER .
            'msU';
    }

    /**
     * Regex for matching and capturing directive name and args when directive has been processed at least once,
     * so it has an open tag, closing tag and existing content inside.
     *
     * @param string|null $directiveName
     * @param string|null $rawDirectiveArgs
     *
     * @return string
     */
    private function getDirectiveStartEndRegex(string $directiveName = null, string $rawDirectiveArgs = null): string
    {
        $directiveName = \is_string($directiveName)
            ? \preg_quote(str: $directiveName, delimiter: self::REGEX_DELIMITER)
            : $this->getDirectiveNamesRegex();
        $rawDirectiveArgs ??= '.*';

        return self::REGEX_DELIMITER .
            '^' . $this->directive_open .
            '(' . $directiveName . ')' . $this->directive_start_suffix .
            '(?:()' . $this->directive_close . '|\s+?(' . $rawDirectiveArgs . ')' . $this->directive_close . ')\s*$' .
            '.*' .
            '^' . $this->directive_open .
            '\1' . $this->directive_end_suffix .
            '(?:\2' . $this->directive_close . '|\s+?\3' . $this->directive_close . ')\s*$' .
            self::REGEX_DELIMITER .
            'msU';
    }

    /**
     * Get directive names expression for regex.
     *
     * @return string
     */
    private function getDirectiveNamesRegex(): string
    {
        return \implode(
            separator: '|',
            array:     \array_map(
                           array: $this->directiveProvider->getDirectiveNames(),
                           callback: static fn(string $directiveName) => \preg_quote(
                               str:       $directiveName,
                               delimiter: self::REGEX_DELIMITER
                           ),
                       )
        );
    }

    /**
     * Get actual directive content.
     *
     * @param string   $directiveName
     * @param string   $rawDirectiveArgs
     * @param array    $directiveArgs
     * @param WikiPage $wikiPage
     *
     * @return string
     */
    private function getDirectiveContent(
        string   $directiveName,
        string   $rawDirectiveArgs,
        array    $directiveArgs,
        WikiPage $wikiPage,
    ): string {
        return self::DIRECTIVE_OPEN . $directiveName . self::DIRECTIVE_START_SUFFIX .
            ' ' . \trim(string: $rawDirectiveArgs) . self::DIRECTIVE_CLOSE .
            PHP_EOL . PHP_EOL .
            $this->directiveProvider->get(directiveName: $directiveName)?->execute(
                directiveArgs: $directiveArgs,
                wikiPage:      $wikiPage
            ) .
            PHP_EOL . PHP_EOL .
            self::DIRECTIVE_OPEN . $directiveName . self::DIRECTIVE_END_SUFFIX .
            ' ' . \trim(string: $rawDirectiveArgs) . self::DIRECTIVE_CLOSE;
    }

    /**
     * Parse directive args to array from raw string.
     *
     * @param string $rawDirectiveArgs
     *
     * @return array
     */
    private function parseDirectiveArgs(string $rawDirectiveArgs): array
    {
        return \array_filter(
            array: \preg_split(
                       pattern: self::REGEX_DELIMITER . '\s+' . self::REGEX_DELIMITER,
                       subject: $rawDirectiveArgs,
                   ),
            callback: static fn(string $directiveArg) => '' !== $directiveArg,
        );
    }
}
