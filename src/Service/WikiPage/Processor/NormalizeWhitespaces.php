<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\WikiPage\Processor;

use Vdshop\WikiJsTools\Contract\Logger;
use Vdshop\WikiJsTools\Contract\WikiPage\Processor as WikiPageProcessor;
use Vdshop\WikiJsTools\Model\WikiPage;
use Vdshop\WikiJsTools\Service\FeatureFlag;
use Vdshop\WikiJsTools\Traits\Path\GetRelativePath;
use Vdshop\WikiJsTools\Traits\Path\IsFeatureFlagEnabled;

/**
 * Class NormalizeLines.
 *
 * Normalize whitespaces (lines are considered whitespaces too).
 */
class NormalizeWhitespaces implements WikiPageProcessor
{
    use GetRelativePath;
    use IsFeatureFlagEnabled;

    private const FF_NAME = 'NORMALIZE_WHITESPACES';

    /**
     * NormalizeWhitespaces constructor.
     *
     * @param FeatureFlag $featureFlag
     * @param Logger      $logger
     */
    public function __construct(
        private readonly FeatureFlag $featureFlag,
        private readonly Logger $logger,
    ) {
    }

    /**
     * @inheritdoc
     */
    public function execute(WikiPage $wikiPage): void
    {
        if (!$this->supports(wikiPage: $wikiPage)) {
            $this->logger->debug(
                message: \get_class($this) .
                         ' does not support ' .
                         $wikiPage->getFileInfo()->getType() .
                         ' ' .
                         $wikiPage->getFileInfo()->getRealPath()
            );

            return;
        }

        if (!$this->isFeatureFlagEnabled()) {
            $this->logger->debug(
                message: \get_class($this) .
                         ' skipped, feature flag ' .
                         FeatureFlag::DEFAULT_PREFIX .
                         self::FF_NAME .
                         ' not enabled.'
            );

            return;
        }

        $origContent = $wikiPage->getContent()->get();
        $processedContent = $origContent;

        // Replace tabs with spaces.
        $processedContent = \preg_replace(
            pattern:     '#\t#',
            replacement: '  ',
            subject:     $processedContent,
        );

        // Remove trailing spaces from lines.
        $processedContent = \preg_replace(
            pattern:     '#^(.*)([ ]+)$#mU',
            replacement: '$1',
            subject:     $processedContent,
        );

        // Add explicit html line breaks before h2 - h6 headers.
        $processedContent = \preg_replace(
            pattern:     '/^(#{2,}.*)$/m',
            replacement: "\n<br>\n\n$1",
            subject:     $processedContent,
        );

        // Ensure empty separator lines before and after h1 - h6 headers.
        $processedContent = \preg_replace(
            pattern:     '/^(#+.*)$/m',
            replacement: "\n\n$1\n\n",
            subject:     $processedContent,
        );

        // Normalize code block lines.
        $processedContent = \preg_replace(
            pattern:     '#^\s*`{3}(\w*)\s*$\n\s*(.*)\s*^\s*`{3}\s*$#msU',
            replacement: "\n```$1\n$2\n```\n",
            subject:     $processedContent,
        );

        // Inline code must be enclosed in single backticks.
        $processedContent = \preg_replace(
            pattern:     '#`{3}(.*)`{3}#U',
            replacement: "`$1`",
            subject:     $processedContent,
        );

        // Remove spaces from lines containing spaces only to be parsed properly as empty lines.
        $processedContent = \preg_replace(
            pattern:     '#^\s+$#m',
            replacement: '',
            subject:     $processedContent,
        );

        // Ensure there are no more than 2 consecutive line breaks.
        $processedContent = \preg_replace(
            pattern:     '#\n{3,}#s',
            replacement: "\n\n",
            subject:     $processedContent,
        );

        // Ensure there are no more than 2 consecutive spaces that do not represent indentation.
        do {
            $processedContent = \preg_replace(
                pattern:     '#([^\s]+)([ ]{2,})([^\s]+)#',
                replacement: '$1 $3',
                subject:     $processedContent,
                count:       $matchesCount
            );
        } while (0 < $matchesCount);

        // Ensure content without leading empty lines.
        // Ensure content ends with a single new trailing line.
        $processedContent = \trim(string: $processedContent) . PHP_EOL;

        if ($origContent !== $processedContent) {
            $wikiPage->getContent()->set($processedContent);
            $this->logger->notice(message: '[!] ' . self::FF_NAME . ' updated content');
            $this->logger->increaseIndentation();
            $this->logger->debug(message: 'from: ' . $origContent);
            $this->logger->debug(message: 'to  : ' . $processedContent);
            $this->logger->decreaseIndentation();
        }
    }

    /**
     * Check if this processor supports given wiki page.
     *
     * @param WikiPage $wikiPage
     *
     * @return bool
     */
    private function supports(WikiPage $wikiPage): bool
    {
        $fileInfo = $wikiPage->getFileInfo();

        return $fileInfo->isFile() && $fileInfo->getExtension() === WikiPage::FILE_EXTENSION;
    }
}
