<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\WikiPage\Processor\Directives;

use SplFileInfo;
use Stringable;
use Vdshop\WikiJsTools\Contract\Directive\Processor as DirectiveProcessor;
use Vdshop\WikiJsTools\Enum\Content\PageTypes as PageTypesEnum;
use Vdshop\WikiJsTools\Enum\WikiPage\Editor as EditorEnum;
use Vdshop\WikiJsTools\Enum\WikiPage\TagKeys;
use Vdshop\WikiJsTools\Enum\WikiPage\TagKeys as TagKeysEnum;
use Vdshop\WikiJsTools\Model\WikiPage;
use Vdshop\WikiJsTools\Service\Provider\Filesystem\Iterator;
use Vdshop\WikiJsTools\Service\WikiPage\Metadata\TagsFactory;
use Vdshop\WikiJsTools\Service\WikiPage\Repository;
use Vdshop\WikiJsTools\Traits\Path\GetRelativePath;

/**
 * Class DirectoryIndex.
 *
 * Process directory index directives.
 */
class DirectoryIndex implements DirectiveProcessor
{
    use GetRelativePath;

    /**
     * DirectoryIndex constructor.
     *
     * @param TagsFactory $tagsFactory
     * @param Iterator    $iteratorProvider
     * @param Repository  $wikiPageRepository
     */
    public function __construct(
        private readonly TagsFactory $tagsFactory,
        private readonly Iterator $iteratorProvider,
        private readonly Repository $wikiPageRepository
    ) {
    }

    /**
     * @inheritdoc
     */
    public function execute(array $directiveArgs, WikiPage $wikiPage): string|Stringable
    {
        if (!$this->supports(fileInfo: $wikiPage->getFileInfo())) {
            return '';
        }

        $wikiPage->getMetadata()->getDescription()->set(
            description: $wikiPage->getMetadata()->getTitle() . ' directory index.'
        );
        $wikiPage->getMetadata()->getTags()->setByKey(
            tagKey:    TagKeysEnum::PAGE_TYPE,
            tagValues: PageTypesEnum::INDEX->value
        );
        $wikiPage->getMetadata()->getPublished()->set(published: true);
        $wikiPage->getMetadata()->getEditor()->set(editor: EditorEnum::MARKDOWN);

        return $this->getContent(
            directoryFileInfo:  $this->getRelatedDirInfo(fileInfo: $wikiPage->getFileInfo()),
            directoryIndexPage: $wikiPage,
        );
    }

    private function getContent(SplFileInfo $directoryFileInfo, WikiPage $directoryIndexPage): string
    {
        $links = [];
        $contentCommonTags = [];

        foreach ($this->iteratorProvider->getFilesArray(
            path: $directoryFileInfo->getRealPath(),
            callbackFilter: static fn(SplFileInfo $current): bool =>
                WikiPage::FILE_EXTENSION === $current->getExtension() &&
                $current->getRealPath() !== $directoryIndexPage->getFileInfo()->getRealPath()
        ) as $childFileInfo) {
            $childPage = $this->wikiPageRepository->get(file: $childFileInfo);

            $contentCommonTags[] = $childPage->getMetadata()->getTags()->get();

            $childTitle = $childPage->getMetadata()->getTitle()->get()
                ?: $childFileInfo->getBasename(suffix: '.' . WikiPage::FILE_EXTENSION);
            $childDescription = $childPage->getMetadata()->getDescription()->get();
            $childUrlPath = \implode(
                separator: '/',
                array:     [
                               'en',
                               $this->getRelativePath(absolutePath: $directoryFileInfo->getRealPath()),
                               $childFileInfo->getBasename(suffix: '.' . WikiPage::FILE_EXTENSION),
                           ]
            );

            $childLink = "- [$childTitle";
            if ('' !== $childDescription) {
                $childLink .= " *$childDescription*";
            }
            $childLink .= "](/$childUrlPath)";

            $links[] = $childLink;
        }

        if (0 < \count($links)) {
            $links[] = '{.links-list}';
            $linksList = \implode(separator: PHP_EOL, array: $links);
        }

        if (0 < \count($contentCommonTags)) {
            $contentTagsObject = $this->tagsFactory->create();

            \array_map(
                array: \array_intersect(...$contentCommonTags),
                callback: static fn(string $tagString) => $contentTagsObject->addFromString(tags: $tagString),
            );

            $contentTagsObject->setByKey(tagKey: TagKeysEnum::PAGE_TYPE, tagValues: PageTypesEnum::CONTENT->value);
            $contentTagsObject->unsetByKey(tagKey: TagKeysEnum::TOPIC);

            $contentTagsLink = $contentTagsObject->getLink();
        }

        $linksList ??= '';
        $contentTagsLink ??= '';

        return <<<"EOD"
        # Browse by content
        
        $linksList
        
        # Browse by tags
        
        [Browse related content by tags]($contentTagsLink).
        EOD;
    }

    /**
     * Check if this processor supports given file info.
     *
     * @param SplFileInfo $fileInfo
     *
     * @return bool
     */
    private function supports(SplFileInfo $fileInfo): bool
    {
        return $fileInfo->isFile() && \is_dir(filename: $this->getRelatedDirPath($fileInfo));
    }

    /**
     * @param SplFileInfo $fileInfo
     *
     * @return SplFileInfo
     */
    private function getRelatedDirInfo(SplFileInfo $fileInfo): SplFileInfo
    {
        return new SplFileInfo(filename: $this->getRelatedDirPath($fileInfo));
    }

    /**
     * @param SplFileInfo $fileInfo
     *
     * @return string
     */
    private function getRelatedDirPath(SplFileInfo $fileInfo): string
    {
        return $fileInfo->getPathInfo()->getRealPath() .
            DIRECTORY_SEPARATOR .
            $fileInfo->getBasename(suffix: '.' . WikiPage::FILE_EXTENSION);
    }
}
