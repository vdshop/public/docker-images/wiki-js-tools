<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\WikiPage;

use InvalidArgumentException;
use RuntimeException;
use SplFileInfo;
use Stringable;
use Vdshop\WikiJsTools\Model\WikiPage as WikiPageModel;
use Vdshop\WikiJsTools\Service\WikiPageFactory;

/**
 * Class Repository.
 *
 * WikiPage model repository.
 */
class Repository
{
    /**
     * @var WikiPageModel[]
     */
    private array $wikiPages = [];

    /**
     * WikiPage constructor.
     *
     * @param WikiPageFactory $wikiPageFactory
     */
    public function __construct(
        private readonly WikiPageFactory $wikiPageFactory,
    ) {
    }

    /**
     * Get WikiPage model.
     *
     * @param SplFileInfo|string|Stringable $file
     *
     * @return WikiPageModel
     */
    public function get(SplFileInfo|string|Stringable $file): WikiPageModel
    {
        $fileInfo = $file instanceof SplFileInfo ? $file : new SplFileInfo(filename: (string)$file);
        $this->validateFileInfo($fileInfo);

        return $this->wikiPages[$fileInfo->getRealPath()] ??= $this->wikiPageFactory->create(fileInfo: $fileInfo);
    }

    /**
     * Save WikiPage model.
     *
     * @param WikiPageModel $wikiPage
     *
     * @return void
     */
    public function save(WikiPageModel $wikiPage): void
    {
        \file_put_contents(
            filename: $wikiPage->getFileInfo()->getRealPath(),
            data:     (string)$wikiPage
        );

        $this->wikiPages[$wikiPage->getFileInfo()->getRealPath()] = $wikiPage;
    }

    /**
     * Validate current file info object.
     *
     * @param SplFileInfo $fileInfo
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    private function validateFileInfo(SplFileInfo $fileInfo): void
    {
        $this->validateFileInfoType(fileInfo: $fileInfo);
        $this->validateFileInfoExtension(fileInfo: $fileInfo);
    }

    /**
     * Validate current file info object type.
     *
     * @param SplFileInfo $fileInfo
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    private function validateFileInfoType(SplFileInfo $fileInfo): void
    {
        if (!$fileInfo->isFile()) {
            try {
                $actualFileType = $fileInfo->getType();
            } catch (RuntimeException) {
                $actualFileType = 'unknown';
            }

            throw new InvalidArgumentException(
                message: \sprintf(
                             'Invalid path type for %s: file expected, %s found',
                             $fileInfo->getFilename(),
                             $actualFileType,
                         )
            );
        }
    }

    /**
     * Validate current file info extension.
     *
     * @param SplFileInfo $fileInfo
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    private function validateFileInfoExtension(SplFileInfo $fileInfo): void
    {
        if (WikiPageModel::FILE_EXTENSION !== $fileInfo->getExtension()) {
            throw new InvalidArgumentException(
                message: \sprintf(
                             'Invalid file extension in file %s, %s expected, %s found',
                             $fileInfo->getRealPath(),
                             WikiPageModel::FILE_EXTENSION,
                             $fileInfo->getExtension()
                         )
            );
        }
    }
}
