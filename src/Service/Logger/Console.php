<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\Logger;

use Psr\Log\LoggerTrait;
use Psr\Log\LogLevel;
use Vdshop\WikiJsTools\Contract\Logger;
use Vdshop\WikiJsTools\Service\FeatureFlag;

/**
 * Class Console.
 *
 * Simple console logger.
 */
class Console implements Logger
{
    use LoggerTrait;

    private const FF_NAME_PREFIX = 'LOG_LEVEL_';

    private const LOG_LEVELS = [
        LogLevel::EMERGENCY,
        LogLevel::ALERT,
        LogLevel::CRITICAL,
        LogLevel::ERROR,
        LogLevel::WARNING,
        LogLevel::NOTICE,
        LogLevel::INFO,
        LogLevel::DEBUG,
    ];

    private int $indentation = 0;

    public function __construct(
        private readonly FeatureFlag $featureFlag,
        private readonly int $indentationStep = 2,
        private readonly string $indentationChar = '-',
    ) {
    }

    /**
     * Increase log indentation.
     *
     * @return void
     */
    public function increaseIndentation(): void
    {
        $this->indentation += $this->indentationStep;
    }

    /**
     * Decrease log indentation.
     *
     * @return void
     */
    public function decreaseIndentation(): void
    {
        $this->indentation = \max(0, $this->indentation - $this->indentationStep);
    }

    /**
     * Log message to console.
     *
     * @inheritdoc
     */
    public function log(mixed $level, string|\Stringable $message, array $context = []): void
    {
        $level = \strtoupper(string: (string)$level);

        if (!$this->isLogLevelEnabled(level: $level)) {
            return;
        }

        echo '[' . \str_pad(string: $level, length: 9, pad_string: ' ', pad_type: STR_PAD_BOTH) . ']: ';

        if ('' !== (string)$message) {
            echo \str_repeat(string: $this->indentationChar, times: $this->indentation) . '- ' . $message;
        }

        echo PHP_EOL;
    }

    /**
     * Check feature flag is enabled for log level.
     * Level must be one of the allowed levels.
     * FF_LOG_LEVEL_DEBUG enables all log levels.
     * FF_LOG_LEVEL_{DEBUG|INFO|NOTICE|...} enables that log level and higher ones.
     *
     * @param string $level
     *
     * @return bool
     *
     * @see self::LOG_LEVELS
     */
    private function isLogLevelEnabled(string $level): bool
    {
        if (!\in_array(needle: \strtolower(string: $level), haystack: self::LOG_LEVELS, strict: true)) {
            return false;
        }

        $ffLevelEnabled = false;

        foreach (\array_reverse(array: self::LOG_LEVELS) as $logLevel) {
            $ffLevelEnabled = $ffLevelEnabled || $this->featureFlag->isEnabled(
                    featureFlagName: self::FF_NAME_PREFIX . \strtoupper(string: $logLevel)
                );

            if (\strtolower(string: $level) === \strtolower(string: $logLevel)) {
                break;
            }
        }

        return $ffLevelEnabled;
    }
}
