<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Traits\Path;

/**
 * Trait GetRelativePath.
 *
 * Extract relative path from root dir for given absolute path.
 */
trait GetRelativePath
{
    /**
     * Extract relative path from root dir for given absolute path.
     *
     * @param string $absolutePath
     *
     * @return string
     */
    private function getRelativePath(string $absolutePath): string
    {
        return \trim(
            string:     \preg_replace(
                            pattern:  '#^' . CONTENT_PATH . '#m',
                            replacement: '',
                            subject: $absolutePath,
                        ),
            characters: DIRECTORY_SEPARATOR
        );
    }
}
