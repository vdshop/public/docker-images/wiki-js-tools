<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Traits\Path;

/**
 * Trait IsFeatureFlagEnabled.
 *
 * Check feature flag is enabled.
 * Assumes:
 * - Constant FF_NAME being defined in class including this trait.
 * - Class property $featureFlag, instance of \Vdshop\WikiJsTools\Service\FeatureFlag.
 */
trait IsFeatureFlagEnabled
{
    /**
     * Check feature flag is enabled.
     *
     * @return bool
     */
    private function isFeatureFlagEnabled(): bool
    {
        return $this->featureFlag->isEnabled(static::FF_NAME);
    }
}
