<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Exception;

use LogicException;

/**
 * Class ProcessorException.
 *
 * Exception to be thrown when logic exception occurs while performing processing.
 * @see https://www.php.net/manual/es/class.logicexception.php
 *
 * For other exception types, use appropriate type directly.
 */
class ProcessorException extends LogicException
{
}
