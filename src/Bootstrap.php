<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools;

use ErrorException;
use Nette\Bootstrap\Configurator;
use Nette\DI\Container;

/**
 * Class Bootstrap.
 */
final class Bootstrap
{
    private static self $instance;

    private static Container $container;

    private static Configurator $configurator;

    /**
     * Private constructor.
     * Avoid class instantiation.
     */
    private function __construct()
    {
    }

    /**
     * Get self instance.
     *
     * @return self
     */
    public static function getInstance(): self
    {
        return self::$instance ??= new self;
    }

    /**
     * Get configured DI container, booting environment in the way if needed.
     *
     * @param string            $appPath
     * @param string            $contentPath
     * @param string            $configPath
     * @param bool|string|array $debugMode
     *
     * @return Container
     *
     * @throws ErrorException
     */
    public function getContainer(
        string            $appPath = APP_PATH,
        string            $contentPath = CONTENT_PATH,
        string            $configPath = APP_PATH . '/config.neon',
        bool|string|array $debugMode = true,
    ): Container {
        return self::$container ??= $this->boot(
            appPath:    $appPath,
            contentPath: $contentPath,
            configPath: $configPath,
            debugMode:  $debugMode,
        )->createContainer();
    }

    /**
     * Boot environment.
     *
     * @param string            $appPath
     * @param string            $contentPath
     * @param string            $configPath
     * @param bool|string|array $debugMode
     *
     * @return Configurator
     *
     * @throws ErrorException
     */
    public function boot(
        string            $appPath = APP_PATH,
        string            $contentPath = CONTENT_PATH,
        string            $configPath = APP_PATH . '/config.neon',
        bool|string|array $debugMode = true,
    ): Configurator {
        $this->configureErrorReporting();
        $this->configureErrorHandler();

        return self::$configurator ??= (new Configurator)
            ->setDebugMode(value: $debugMode)
            ->setTempDirectory(path: $appPath . '/var/tmp')
            ->addConfig(config: $configPath)->addConfig(
                config: [
                        'parameters' => [
                            'appDir'     => $appPath,
                            'srcDir'     => $appPath . DIRECTORY_SEPARATOR . 'src',
                            'contentDir' => $contentPath,
                        ],
                    ]
            );
    }

    /**
     * Configure error reporting.
     *
     * @return void
     */
    private function configureErrorReporting(): void
    {
        \error_reporting(error_level: E_ALL);
    }

    /**
     * Configure error handler.
     *
     * @return void
     *
     * @throws ErrorException
     */
    private function configureErrorHandler(): void
    {
        \set_error_handler(
            callback: static function ($errno, $errstr, $errfile, $errline): void {
                if (\error_reporting() & $errno) {
                    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
                }
            },
            error_levels: \error_reporting(),
        );
    }
}
