<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Contract\Project;

use Vdshop\WikiJsTools\Exception\ProcessorException;

/**
 * Interface Processor.
 *
 * Perform some kind of processing on the project.
 */
interface Processor
{
    /**
     * Perform actual project processing.
     *
     * @throws ProcessorException
     */
    public function execute(): void;
}
