<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Contract;

use Psr\Log\LoggerInterface;

/**
 * Interface Logger.
 *
 * Extend PSR logger with additional features.
 */
interface Logger extends LoggerInterface
{
    /**
     * Increase log indentation.
     */
    public function increaseIndentation(): void;

    /**
     * Decrease log indentation.
     */
    public function decreaseIndentation(): void;
}
