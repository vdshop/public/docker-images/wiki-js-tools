<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Contract\Directive;

use Stringable;
use Vdshop\WikiJsTools\Exception\ProcessorException;
use Vdshop\WikiJsTools\Model\WikiPage;

/**
 * Interface Processor.
 *
 * Perform some kind of processing on a given path.
 */
interface Processor
{
    /**
     * Perform actual directive processing.
     *
     * @param string[] $directiveArgs
     * @param WikiPage $wikiPage
     *
     * @return string|Stringable
     *
     * @throws ProcessorException
     */
    public function execute(array $directiveArgs, WikiPage $wikiPage): string|Stringable;
}
