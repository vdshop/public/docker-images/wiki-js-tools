<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Contract\Directive\Processor;

use Vdshop\WikiJsTools\Contract\Directive\Processor as DirectiveProcessor;

/**
 * Interface Provider.
 *
 * Directive processor provider.
 */
interface Provider
{
    /**
     * Get directive processor by name, if any.
     */
    public function get(string $directiveName): ?DirectiveProcessor;

    /**
     * Get all registered processors.
     *
     * @return DirectiveProcessor[]
     */
    public function getAll(): array;

    /**
     * Get directive names.
     *
     * @return string[]
     */
    public function getDirectiveNames(): array;
}
