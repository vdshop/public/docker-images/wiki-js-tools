<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Contract\Path;

use SplFileInfo;
use Vdshop\WikiJsTools\Exception\ProcessorException;

/**
 * Interface Processor.
 *
 * Perform some kind of processing on a given path.
 */
interface Processor
{
    /**
     * Perform actual path processing.
     *
     * @throws ProcessorException
     */
    public function execute(SplFileInfo $fileInfo): void;
}
